package com.academiasmoviles.lab01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcesar.setOnClickListener {

            tvResultado.text = when(edtEdad.text.toString().toInt()) {
                in 0..17 -> {
                    "Usted es menor de edad"
                }
                in 18..100 ->{
                    "Usted es mayor de edad"
                }
                else ->{
                    "Edad erronea"
                }
            }
        }
    }
}